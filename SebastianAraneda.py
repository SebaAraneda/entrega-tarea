# -*- coding: utf-8 -*-
"""
Created on Wed Nov 01 17:44:43 2017

@author: Sebastian
"""
def encontrarPadre(graph, nodo):
    for key in graph.keys():
        if key > nodo:
            break
        for conexion in graph[key]:
                 if conexion ==  nodo:
                     return key
    return False
                     
def encontrarAbuelo(graph, nodo):
    P = encontrarPadre(graph, nodo)
    if P != False:
        return encontrarPadre(graph, P)
    else:
        return False
        

def encontrarAncestro(graph, nodo, nivel):
    ancestro = nodo
    for i in range(nivel):
        ancestro = encontrarPadre(graph, ancestro)
        if ancestro == False:
            return False
    return ancestro
    
def isAncestro(graph, hijo, ancestro):
    nivel = 1
    Maxnivel = getMaxNivel(graph)
    while True:
        nodo2 = encontrarAncestro(graph, hijo, nivel)
        if nodo2 == ancestro:
            return True
        elif nodo2 == False:
            return False
        elif nivel == Maxnivel+1:
            return False
        else:
            nivel += 1
    
def getMaxNivel(graph):
    maxNodo = graph.keys()[-1]
    n = 0
    stop = True
    nodoAIterar = maxNodo
    while stop:
        nodoAIterar = encontrarPadre(graph, nodoAIterar)
        if nodoAIterar != False:
            n += 1
        else:
            stop = False
    return n
import copy

nombreArchivo = str(raw_input("Ingrese el nombre del archivo de entrada =>"))
ArchivoE = open(nombreArchivo)
nombreSalida = str(raw_input("Ingrese el nombre del archivo de salida = >"))
entrada = ArchivoE.readline().strip().split(' ')    
ArchivoE.close()
t = int(entrada[0])
b = int(entrada[1])
f = int(entrada[2])
c = int(entrada[3])

graph = {}
alternador = True

for i in range(1, t+2):
    graph[i] = []
    if i == 1 and t == 1:
        graph[i] = [2]
        break
    elif i == 1 and t > 2:
        graph[i] = [2, 3]
    
    elif i > 3:
        graph[i-2].append(i) 
keysReversed = graph.keys()
keysReversed.reverse()
maxNivel = getMaxNivel(graph)
graphParaAncestros = copy.deepcopy(graph)
n = 0
for k in range(b):
    while True:
        NodoIterando = keysReversed[n]
        ancestro = encontrarAncestro(graphParaAncestros, NodoIterando, maxNivel)
        if ancestro == False:
            maxNivel -= 1
            if maxNivel < 2:
                print "No existe el minimo de espacio para los back edges"
                quit()
            else:
                n = 0
                continue
        else:
            graph[NodoIterando].append(ancestro)
            n = n+1
            break
f_puestos = 0
for nodo in range(1, t+2):
    if f_puestos == f:
        break

    for nodoHijo in keysReversed:
        if isAncestro(graphParaAncestros, nodoHijo, nodo) and nodoHijo > nodo:
            if graph[nodoHijo].__contains__(nodo) or graph[nodo].__contains__(nodoHijo):
                continue
            else:
                graph[nodo].append(nodoHijo)
                f_puestos += 1
        else:
            continue
        if f_puestos == f:
            break
    
if f_puestos != f:
    print "No existe el minimo de espacio para los forward edges"
    quit()

c_puestos = 0
for nodo in range(1, t+2):
    if c_puestos == c:
        break

    for nodoHijo in keysReversed:
        if not isAncestro(graphParaAncestros, nodoHijo, nodo) and nodoHijo != nodo:
            if graph[nodoHijo].__contains__(nodo) or graph[nodo].__contains__(nodoHijo):
                continue
            else:
                graph[nodo].append(nodoHijo)
                c_puestos += 1
        else:
            continue
        if c_puestos == c:
            break

if c_puestos != c:
    print "No existe el minimo de espacio para los cross edges"
    quit()

salida = open(nombreSalida, 'w+')
salida.write(str(keysReversed[0])+"\n")
for nodo in range(1, t+2):
    out = ""
    out = out + str(len(graph[nodo]))+" "
    graph[nodo].sort()
    for nodoConectado in graph[nodo]:
        out = out + str(nodoConectado)
        if nodoConectado != graph[nodo][-1]:
            out = out + " "
        else:
            if nodo != t+1:
                out = out +"\n"
    salida.write(out)
salida.close()
    
    